from rdkit import Chem
from rdkit.Chem import AllChem

test1=('OCC(O)C(=O)O')
test2=('OCC(O)CO')
glucose_lineaire=('OCC(O)C(O)C(O)C(O)C=O')
fructose=('C(C1C(C(C(O1)(CO)O)O)O)O')

#Alcool vers cetone/aldéhyde
def cetone(smile):
    """returns all the products of a first oxidation to ketone.

    Arguments:
    smile -- string SMILES of the starting molecule
    """
    patt=Chem.MolFromSmarts('[OH][CH,CH2]')
    rxn = AllChem.ReactionFromSmarts('[CH,CH2:1][OH:2]>>[C:1]=[O:2].O')
    listecetone=[smile]
    for smile in listecetone:
        if Chem.MolFromSmiles(smile).HasSubstructMatch(patt):
            ps = rxn.RunReactants((Chem.MolFromSmiles(smile), ))
            for i in range(len(ps)):
                listecetone.append(Chem.MolToSmiles(ps[i][0]))
    del listecetone[0]
    s=set(listecetone) #removes identical smiles from the list
    listecetone2=list(s)
    f=open("cetone.smile","w+")
    for elem in listecetone2:
        f.write('{}\n'.format(elem))
    f.close
    print(listecetone2)
    return listecetone2

#Forme énol
def enol(smile):
    """returns all the products of a first oxidation in enol form.

    Arguments:
    smile -- string SMILES of the starting molecule
    """
    patt=Chem.MolFromSmarts('[CH,CH2,CH3][CH,CH2,CH3][OH]')
    rxn = AllChem.ReactionFromSmarts('[CH,CH2,CH3:1][CH,CH2,CH3:2][OH:3]>>[C:1]=[C:2][OH:3].O')
    listeenol=[smile]
    for smile in listeenol:
        if Chem.MolFromSmiles(smile).HasSubstructMatch(patt):
            ps = rxn.RunReactants((Chem.MolFromSmiles(smile), ))
            for i in range(len(ps)):
                listeenol.append(Chem.MolToSmiles(ps[i][0]))
    del listeenol[0]
    s=set(listeenol) #supprime les smiles identiques de la liste
    listeenol2=list(s)
    f=open("enol.smile","w+")
    for elem in listeenol2:
        f.write('{}\n'.format(elem))
    f.close
    print(listeenol2)
    return listeenol2



#Alcool vers acide carbo
def acide(smile):
    """returns all the products of a second oxidation to carboxylic acide.

    Arguments:
    smile -- string SMILES of the starting molecule
    """
    patt=Chem.MolFromSmarts('O=[CH]')
    rxn = AllChem.ReactionFromSmarts('[CH:1]=[O:2]>>[C:1](=[O:2])O')
    listeacide=cetone(smile)
    nbcetone=len(listeacide)
    for smile in listeacide:
        if Chem.MolFromSmiles(smile).HasSubstructMatch(patt):
            ps = rxn.RunReactants((Chem.MolFromSmiles(smile), ))
            for i in range(len(ps)):
                listeacide.append(Chem.MolToSmiles(ps[i][0]))
    del listeacide[0:nbcetone]
    s=set(listeacide)
    listeacide2=list(s)
    f=open("acide.smile","w+")
    for elem in listeacide2:
        f.write('{}\n'.format(elem))
    f.close
    print(listeacide2)
    return listeacide2


#Elimination d'eau
def deshydratation(smile):
    """returns all the products from dehydration.

    Arguments:
    smile -- string SMILES of the starting molecule
    """
    patt=Chem.MolFromSmarts('[CH,CH2,CH3][CH,CH2,CH3][OH]')
    rxn = AllChem.ReactionFromSmarts('[CH,CH2,CH3:1][CH,CH2,CH3:2][OH:3]>>[C:1]=[C:2].[O:3]')
    listedes=[smile]
    for smile in listedes:
        if Chem.MolFromSmiles(smile).HasSubstructMatch(patt):
            ps = rxn.RunReactants((Chem.MolFromSmiles(smile), ))
            for i in range(len(ps)):
                listedes.append(Chem.MolToSmiles(ps[i][0]))
    del listedes[0]
    s=set(listedes) #supprime les smiles identiques de la liste
    listedes2=list(s)
    f=open("deshydratation.smile","w+")
    for elem in listedes2:
        f.write('{}\n'.format(elem))
    f.close
    print(listedes2)
    return listedes2



#cetone(test2)
#enol(test2)
#acide(test2)
#deshydratation(test2)

