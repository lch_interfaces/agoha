(English version bellow)

AGOHA - Automatisation de Gaussian pour l'Oxydation et la désHydratation d'Alcools
===

But du projet
---

Le but de ce projet est de :

1. Générer les intermédiaires issus de l'oxydation d'un sucre
1. Générer les conformères de ces intermédiaires d'intérêt
1. Générer les fichiers d'entrée Gaussian .com des conformères les plus bas en énergie et lancer les calculs sous Gaussian automatiquement
1. Extraire les données thermodynamiques (H, G) des fichiers de sortie .log et calculer l'enthalpie libre de chaque réaction pour chaque intermédiaire



Composants du projet
---

Le projet comporte quatre fichiers :

- AGOHA.py : le fichier principal
- reaction.py : fichier contenant la définition des réactions
- extract.py : le fichier de post-traitement pour extraire les données et calculer ∆rG
- le script de soumission Gaussian

Pré-requis
--

- RDKit
- Clic
- Gaussian



Utilisation
---


Pour utiliser ce programme, il est nécessaire de cloner le projet. Ensuite, dans un terminal tapez :

`python AGOHA.py -i ’SMILES’`

Cela permettra de générer tous les intermédiaires, leurs conformères et de lancer les calculs avec Gaussian.

Pour obtenir de l'aide sur le lancement du programme, tapez :
`python AGOHA.py --help`


Une fois que les calculs Gaussian sont terminés, tapez dans le terminal :
`python extract.py`

Cela générera un fichier de sortie nommé valeur.dat avec toutes les données, y compris la réaction chimique, le numéro du produit et du conformère (appelé géométrie), et ∆rG en kJ/mol.



Project Goal
--

The goal of this project is to:

1. Generate intermediates resulting from the oxidation, dehydration of sugar
1. Generate conformers of these intermediates of interest
1. Generate Gaussian .com input files of the lowest energy conformers and automatically launch calculations under Gaussian
1. Extract thermodynamic data (H, G) from .log output files and calculate the free enthalpy of each reaction for each intermediate

Project Components
--
The project consists of four files:

- AGOHA.py: the main file
- reaction.py: file containing the definition of reactions
- extract.py: the post-processing file to extract data and calculate ΔrG
- the Gaussian submission script

Prerequisites
--
- RDKit
- Clic
- Gaussian



Usage
--
To use this program, you need to clone the project. Then, in a terminal type:

`python AGOHA.py -i ’SMILES’`

This will generate all intermediates, their conformers, and launch calculations with Gaussian.

To get help on launching the program, type:
`python AGOHA.py --help`

Once the Gaussian calculations are complete, type in the terminal:
`python extract.py`

This will generate an output file named valeur.dat with all data including the chemical reaction, product and conformer number (called geometry), and ΔrG in kJ/mol.
