import subprocess
import os
import reaction as rea
from rdkit import Chem
import pickle

def count_files(directory, extension):
    """give number of files in a directory

    Arguments:
    directory -- string path to the directory
    extension -- string name of the extension
    """
    return len([f for f in os.listdir(directory) if f.endswith(extension)])

def extract_energie(fichier):
    """extract from a Gaussian file the thermodynamic data

    Arguments:
    fichier -- string path to the file.log to analyze
    """
    with open(fichier,"r") as f:
        for line in f:
            if "Sum of electronic and thermal Energies=" in line :
                energie_line=line
                mots=line.split()
                energie=float(mots[6])
            if "Sum of electronic and thermal Enthalpies=" in line :
                enthalpie_line=line
                mots=line.split()
                enthalpie=float(mots[6])
            if "Sum of electronic and thermal Free Energies=" in line :
               G_line=line
               mots=line.split()
               G=float(mots[7])
   # print(energie_line)
   # print(enthalpie_line)
   # print(G_line)
    return energie,enthalpie,G


# Extraction of energy data for each product
with open("liste.pkl","rb") as f :
    liste=pickle.load(f) #reopening the previously generated list
print(liste)
cwd=os.getcwd()
listeG=[]
for name,listesmile in liste:
    print(listesmile)
    os.chdir(cwd+'/{}'.format(name))
    j=1
    for molecule in listesmile:
        print(molecule)
        os.chdir(cwd+'/{}/produit{}'.format(name,j))
        print('produit{}'.format(j))
        for i in range(1,1+count_files(os.getcwd(),'.log')):
            print('geometrie{}'.format(i))
            try:
                G=extract_energie("geometrie{}.log".format(i))[2]
                listeG.append([molecule,'geometrie{}'.format(i),G,name,'produit{}'.format(j)])
            except UnboundLocalError:
                print("energie not found, skipping the file geometrie{}.log".format(i))
                continue
        j=j+1
os.chdir(cwd)



#Determination of the energy of the most stable starting reactant
nb_depart=count_files(cwd+'/depart/produit1','.log')
G_depart=[]
for molecule,geometrie,G,name,produit in listeG[0:nb_depart]:
    if molecule == listeG[0][0]:
        G_depart.append(G)   
E_depart=min(G_depart)
Mol_depart=listeG[0][0]

E_O2=-150.343816 #level B3LYP/6-31+G**
E_H2O=-76.430399 #level B3LYP/6-31+G**
print("Starting molecule is ",Mol_depart,"with min energy ",E_depart)

def count_atoms(smiles):
    """count the C, H and O atoms in a SMILES

    Arguments:
    smile -- string SMILES of the molecule
    """
    mol = Chem.MolFromSmiles(smiles)
    mol = Chem.AddHs(mol)
    a = len(mol.GetSubstructMatches(Chem.MolFromSmarts("C"))) # number of carbon atoms
    b = len(mol.GetSubstructMatches(Chem.MolFromSmarts("[H]"))) # number of hydrogen atoms
    c = len(mol.GetSubstructMatches(Chem.MolFromSmarts("O"))) # number of oxygen atoms
    return a, b, c

a,b,c = count_atoms(Mol_depart)

for molecule,geometrie,G,name,produit in listeG[nb_depart:]:
    k,l,m = count_atoms(molecule)
    nb_H2O=(b-l)/2 #number of water molecules
    nb_O2=(m+nb_H2O-c)/2 #number of dioxygen molecules
    DeltaG=G+nb_H2O*E_H2O-E_depart-nb_O2*E_O2
    DeltaGkjmol=2625.5*DeltaG #DeltaG in kJ/mol
    with open('valeur.dat', "a") as f:
        f.write(Mol_depart + ' + ' + str(nb_O2) + ' O2 --> ' + molecule + ' ' + produit + ' ' +geometrie + ' + ' + str(nb_H2O) + ' H2O '+ name + ' '+' DeltaG= ' + str(DeltaGkjmol) + '\n')
         








