#!/bin/bash
#SBATCH -J glucose
#SBATCH -p Lake 
#SBATCH -o %j.out
#SBATCH -e %j.err
#SBATCH --nodes=1
###SBATCH --exclusive
#SBATCH --ntasks=1
###SBATCH --mem=50G
#SBATCH --mem-per-cpu=3000
#SBATCH --cpus-per-task=8
#SBATCH --time=04:00:00

### Utilisation sbatch sub_g16_slumr.sh fichier (sans le .com a la fin)
###

#
module purge
##module use /applis/PSMN/debian11/E5/modules/all
module use /applis/PSMN/debian11/Lake/modules/all/
module use /home/tjiang/modules/lmod/debian11
module load gaussian/g16-avx

source $g16root/g16/bsd/g16.profile
export Gaussian=$g16root/g16/g16

job=$1
ExtIn=com
ExtOut=log

#if [[ -d "/scratch/Cascade" ]]
#then
    SCRATCHDIR=$(mktemp -d)
#else
#    echo "/scratch not found, cannot create ${SCRATCHDIR}"
#    exit 1
#fi

mkdir ${SCRATCHDIR}

CalcDir=${SCRATCHDIR}
export GAUSS_SCRDIR=${CalcDir}
HOMEDIR="$SLURM_SUBMIT_DIR"

NChk=` grep -i "chk" ${job}.${ExtIn} | head -1 | sed 's/=/ /g' | awk '{print $2}'`
if [ "$NChk" != "" ]
then
NChk=` basename $NChk .chk`.chk
fi
if [[ -s ${HOMEDIR}/${NChk} ]]
then
    cp ${HOMEDIR}/${NChk} ${CalcDir}/${NChk}
fi
if [[ -s ${HOMEDIR}/${NChk}.gz ]]
then
    cp ${HOMEDIR}/${NChk}.gz ${CalcDir}/${NChk}.gz
    gunzip ${CalcDir}/${NChk}.gz
fi

cp ${HOMEDIR}/${job}.${ExtIn} ${CalcDir}/

if [[ -s ${HOMEDIR}/${job}.${ExtOut} ]]
then
    Ext=1
    while [[ -s ${HOMEDIR}/${job}.${ExtOut}_${Ext} ]]
    do
        let Ext=Ext+1
    done
    mv ${HOMEDIR}/${job}.${ExtOut} ${HOMEDIR}/${job}.${ExtOut}_${Ext}
fi

cd ${CalcDir}
ls -al
${Gaussian} < ${job}.${ExtIn} > ${HOMEDIR}/${job}.${ExtOut}

cp * ${HOMEDIR}/

if [[ -s ${NChk} ]]
then
    gzip -9 ${NChk}
    cp ${NChk}.gz ${HOMEDIR}/
fi

