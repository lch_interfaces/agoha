### AGOHA programm
## by Nicolas TAVERNIER - ENS de Lyon
## see gitlab.com/lch_interfaces/agoha.git for more details

import click
import os
import pickle
import numpy as np
import subprocess
import reaction as rea
from rdkit import Chem
from rdkit.Chem import rdDistGeom
from rdkit.Chem import AllChem
from rdkit.Chem import rdMolAlign


def fichiercom(input,theorie):
    """Generation of Gaussian input files.

    Arguments:
    input -- integer, number of the molecule to optimise
    theorie -- string, theory level
    """
    geo=open("geometries/geometrie{}.xyz".format(input),'r')
    geocont=geo.readlines()
    with open("geometrie{}.com".format(input), "w") as fcom:
        fcom.write("%NProcShared=8\n%mem=8800MB\n#P {} Opt Freq\n\n Optimisation du conformere\n\n0 1\n".format(theorie))
        for g in range(2,len(geocont)):
            fcom.write(geocont[g])
        fcom.write("\n")   
    geo.close()
    subprocess.call('sbatch ../../sub_g16_slurm.sh geometrie{}'.format(input), shell=True)
    print('optimisation de la geometrie{}'.format(input))
    


@click.command()
@click.option('--input', '-i', help='enter the SMILES within quotation marks', required=True)
@click.option('--output', '-o', default='gen_confs.sdf')
@click.option('--prunermsthresh', '-t', default=0.1, type=float)#, help='Retain only the conformations out of ‘numConfs’')
@click.option('--numconf', default=50, type=int)
@click.option('--add_ref', '-r', default=False, type=bool)
@click.option('--nbconf', '-nb', help='Max. number of conformers per molecule to optimize with Gaussian', default=10)
@click.option('--diffener', '-dE', help='Max. energy difference between conformers', default=3)
@click.option('--theorie', '-th', help='level of theory functional/basis set', default='B3LYP/6-31+G**')
def confgen(input, output, prunermsthresh, numconf, add_ref,nbconf,diffener,theorie):
    depart=Chem.MolFromSmiles(input)
    liste=[['depart',[input]],['acide',rea.acide(input)],['cetone',rea.cetone(input)],['enol',rea.enol(input)],['deshydratation',rea.deshydratation(input)]]
    print(liste)
    with open("liste.pkl","wb") as f:
        pickle.dump(liste, f) #saving the list for later
    for name,listesmile in liste:
        print(listesmile)
        subprocess.call('mkdir {}'.format(name), shell=True)
        cwd=os.getcwd()
        os.chdir(cwd+'/{}'.format(name))
        j=1
        for molecule in listesmile:
            print(molecule)
            subprocess.call('mkdir produit{}'.format(j), shell=True)
            os.chdir(cwd+'/{}/produit{}'.format(name,j))
            j=j+1
            mol = Chem.AddHs(Chem.MolFromSmiles(molecule), addCoords=True) #add hydrogen to the molecule
            refmol = Chem.AddHs(Chem.Mol(mol))
            param = rdDistGeom.ETKDGv2()
            param.pruneRmsThresh = prunermsthresh
            cids = rdDistGeom.EmbedMultipleConfs(mol, numconf, param) #returns the conformers
            mp = AllChem.MMFFGetMoleculeProperties(mol, mmffVariant='MMFF94s')
            AllChem.MMFFOptimizeMoleculeConfs(mol, numThreads=0, mmffVariant='MMFF94s')
            w = Chem.SDWriter(output)#initialize the output file
            if add_ref: #reference
                refmol.SetProp('CID', '-1')
                refmol.SetProp('Energy', '')
                w.write(refmol)
            res = []

            for cid in cids:
                ff = AllChem.MMFFGetMoleculeForceField(mol, mp, confId=cid)
                e = ff.CalcEnergy() #energies in kcal/mol
                res.append((cid, e))
            sorted_res = sorted(res, key=lambda x:x[1]) #sort the conformers by increasing energy
            rdMolAlign.AlignMolConformers(mol) #the first conformation becomes the ref
            subprocess.call('mkdir geometries', shell=True)
            i=1
            for cid, e in sorted_res:
                mol.SetProp('CID', str(cid))
                mol.SetProp('Energy', str(e))
                w.write(mol, confId=cid) 
                Chem.MolToXYZFile(mol,"geometries/geometrie{}.xyz".format(i),cid) #creation of xyz file
                i=i+1
            w.close()
            emin=sorted_res[0][1]
            print(emin)
            i=1
            for cid, e in sorted_res :
                if e-emin<=diffener and i<=nbconf:
                    fichiercom(i,theorie) #creation of Gaussian files for conformers in the energy interval
                if i == nbconf :
                    break
                i=i+1  
            os.chdir(cwd+'/{}'.format(name))
        os.chdir(cwd)






if __name__=='__main__':
    confgen()



